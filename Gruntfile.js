module.exports = function(grunt){
	grunt.initConfig({
		clean: {
			dist: ['dist/']
		},
		bower: {
			install: {
				options: {
					layout: 'byType',
					targetDir: './dist/lib/'
				}
			}
		},
		includes: {
			build: {
				cwd: '.',
				src: ['scripts/**/*.js', 'css/**/*.css'],
				dest: 'dist/',
				options: {
					banner: '<!-- Automatic Includes -->'
				}
			}
		},
		includeSource: {
			options: {
				basePath: '.',
				includePath: ['dist']
			},
			myTarget: {
				files: {
					'dist/index.html' : 'index.html'
				}
			}
		},
		connect: {
			server: {
				options: {
					port: 3000,
					base: 'dist/',
					keepalive: true,
					open: true,
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-includes');
	grunt.loadNpmTasks('grunt-bower-task');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-include-source');
	grunt.loadNpmTasks('grunt-contrib-connect');

	grunt.registerTask('build', ['clean', 'bower', 'includes', 'includeSource',]);
	grunt.registerTask('default', ['build']);
}